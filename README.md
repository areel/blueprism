## Assumptions

Node and Yarn are globally installed

## Installation

Issue the following commands to install demo dependencies and execute demo

```zsh
# command may take a minute or two to complete

yarn

# when dependencies have been installed execute

yarn dev
```

This should start an instance of the default browser and load the demo from localhost:8081 via a webpack local server.

### Extra work:

I have been very impressed with the [Cypress](https://www.cypress.io) Testing framework on current project decided to use it to add some UI tests to codebase. To see it in action (albeit somewhat fast) leave the yarn dev process running and issue the following command in a new shell process.

```
yarn ui:tests
```

this should open a Cypress console window allowing you to execute the tests within

```
blue-prism.spec.js
```

apart from displaying a browser with the Demo UI and displaying the tests being executed, it also produces code coverage information which can be viewed by opening the `index.html` file located at

```
<<project-folder>>\coverage
```

## Comments on Demo Development and final code base

First comment, very nice task, enjoyed working on it.

### Outstanding work:

1. Code Polish: magic strings, did not create a constants module, preventing theming. Feel naming of components could still be improved and some refactoring of components (eg move AssignmentPanel out of StatusCard).

2. some UI artefacts not implemented (bold text within progress bar text, play/pause separators not touching play/pause icon circle, top and bottom of menu doesnt show hover color if top/bottom menu item hovered over)
3. minimum accessibility

### Extra work:

Been impressed with the [Cypress](https://www.cypress.io) Testing framework on current project decided to use it to add some UI tests to codebase. To see it in action (albeit somewhat fast) leave the yarn dev process running and issue the following command in a new shell process.

```
yarn ui:tests
```

this should open a Cypress console window allowing you to execute the tests within

```
blue-prism.spec.js
```

apart from displaying a browser with the Demo UI and displaying the tests being executed, it also produces code coverage information which can be viewed by opening the `index.html` file located at

```
<<project-folder>>\coverage
```

### Process followed.

Studied the sketch file contents
Decided to check that all demo artefacts had been sent via the agent.
Agent confirmed demo artefacts consisted of single file.

Studying the sketch file I was looking for individual components, resisting temptation to investigate the sketch layers.
Done rough pen and paper sketch of core components
Then studied any key time / process information in diagram sequence.

When I felt I had an understanding of the UI in terms of components and one or two use cases (select a task, set its priority, observe it being started and process being reported, pause / resume task, increase/decrease assigned workers) I then compared my thoughts to the sketch layer hierarchy, confirmed component/atom/modecule/organism was in line.

Then started coding on the basis that the UI was to be implemented as close to the sketch document as possible.
