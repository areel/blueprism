describe("When Demo page loaded", function() {
  beforeEach(() => {
    cy.visit("/");
  });

  it("should have Navigation Bar Search available", function() {
    cy.get("[data-testid=task-search]").should("be.visible");
  });

  it("should permit setting Task priority", () => {
    cy.get("[data-testid=assignment-text]")
      .last()
      .contains("0/25");
    cy.get("[data-testid=assignmentStatusButton]")
      .last()
      .should("be.visible")
      .click();
    cy.get("[data-testid=auto-assign-menu-item]")
      .should("be.visible")
      .click();
    cy.get("[data-testid=priority-2]")
      .last()
      .should("be.visible")
      .click();
    cy.get("[data-testid=assignment-text]")
      .last()
      .contains("6/25");
  });
  it("should permit pausing a running Task via play/pause button", () => {
    cy.get("[data-testid=assignment-text]")
      .first()
      .contains("13/25");
    cy.get("[data-testid=pause-button]")
      .first()
      .should("be.visible")
      .click();
    cy.get("[data-testid=play-button]")
      .first()
      .should("be.visible");
    cy.get("[data-testid=assignment-text]")
      .first()
      .contains("0/25");
  });
  it("should permit increasing and decreasing a running Tasks assigned workers", () => {
    cy.get("[data-testid=assignment-text]")
      .first()
      .contains("13/25");
    cy.get("[data-testid=decrease-worker-count-button]")
      .first()
      .should("be.visible")
      .click();
    cy.get("[data-testid=assignment-text]")
      .first()
      .contains("12/25");
    cy.get("[data-testid=increase-worker-count-button]")
      .first()
      .should("be.visible")
      .click()
      .click();
    cy.get("[data-testid=assignment-text]")
      .first()
      .contains("14/25");
  });
});
