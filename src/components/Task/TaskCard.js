import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import ProcessCard from "./Process/ProcessCard";
import StatusCard from "./Status/StatusCard";

const useStyles = makeStyles(theme => ({
  processCard: {},
  taskCardRoot: {
    width: "50%",
    margin: "10px auto 40px auto",
    minWidth: "650px",
    display: "flex",
    borderRadius: "10px 10px 20px 10px"
  },
  rounded: {
    backgroundColor: "blue"
  },
  playPauseButton: {}
}));

const TaskCardContext = React.createContext([{}, () => {}]);

const TaskCard = props => {
  const [state, setState] = useState(props.card);
  const classes = useStyles();
  return (
    <TaskCardContext.Provider value={[state, setState]}>
      <div>
        <Paper
          elevation={3}
          className={classes.taskCardRoot}
          rounded={classes.rounded}
        >
          <ProcessCard />
          <StatusCard />
        </Paper>
      </div>
    </TaskCardContext.Provider>
  );
};
export { TaskCardContext, TaskCard };
