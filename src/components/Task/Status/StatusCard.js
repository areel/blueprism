import React, { useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, IconButton } from "@material-ui/core";
import MinusIcon from "@material-ui/icons/RemoveCircleOutline";
import AddIcon from "@material-ui/icons/AddCircleOutline";
import AssignmentStatus from "./AssigmentStatus";
import PriorityPanel from "./PriorityPanel";
import ProcessPlayPause from "../Process/ProcessPlayPause";
import { TaskCardContext } from "../TaskCard";

const useStyles = makeStyles(theme => ({
  statusCardRoot: {
    display: "grid",
    gridTemplateColumns: "fit-content(50px) auto",
    width: "20%",
    minWidth: "200px"
  },
  statusTitle: {
    textAlign: "center",
    margin: "5px 0 0"
  },
  assignmentText: {
    fontSize: "20px",
    fontWeight: "bold"
  },
  assignmentRoot: {
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginTop: "-5px"
  },
  assignmentButton: {
    color: "#005285",
    fontSize: "small"
  }
}));

const StatusTitle = props => {
  const classes = useStyles();
  return (
    <Typography className={classes.statusTitle}>Workers assigned</Typography>
  );
};

const AssignmentPanel = props => {
  const [state, setState] = useContext(TaskCardContext);
  const classes = useStyles();
  const increaseWorkerCount = () => {
    let newValue = state.assigned + 1;
    newValue = newValue > 25 ? 25 : newValue;
    setState({ ...state, assigned: newValue });
  };
  const decreaseWorkerCount = () => {
    let newValue = state.assigned - 1;
    newValue = newValue < 0 ? 0 : newValue;
    setState({ ...state, assigned: newValue });
  };
  return (
    <div className={classes.assignmentRoot}>
      <IconButton
        data-testid="decrease-worker-count-button"
        onClick={decreaseWorkerCount}
        aria-label="Decrease worker count"
      >
        <MinusIcon className={classes.assignmentButton} />
      </IconButton>
      <Typography
        data-testid="assignment-text"
        className={classes.assignmentText}
      >
        {state.assigned}/25
      </Typography>
      <IconButton
        data-testid="increase-worker-count-button"
        onClick={increaseWorkerCount}
        aria-label="Increase worker count"
      >
        <AddIcon className={classes.assignmentButton} />
      </IconButton>
    </div>
  );
};

export default function StatusCard(props) {
  const classes = useStyles();
  const { className, ...other } = props;
  return (
    <div className={`${className} ${classes.statusCardRoot}`}>
      <div style={{ gridColumnStart: 1 }}>
        <ProcessPlayPause style={{ marginTop: "40px" }} />
      </div>
      <div style={{ gridColumnStart: 2 }}>
        <StatusTitle />
        <AssignmentPanel />
        <PriorityPanel disablePointerEvents={true} />
        <AssignmentStatus />
      </div>
    </div>
  );
}
