import React, { useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import { IconButton } from "@material-ui/core";
import SettingsIcon from "@material-ui/icons/Settings";
import StatusMenu from "./StatusMenu";
import { TaskCardContext } from "../TaskCard";
const useStyles = makeStyles(theme => ({
  assignmentStatusRoot: {
    display: "flex",
    justifyContent: "left",
    alignItems: "center",
    backgroundColor: "#005285",
    color: "white",
    borderRadius: "0 0 20px 0",
    marginLeft: "-16px"
  },
  assignmentStatusButton: {
    color: "white",
    fontSize: "small",
    padding: "3px"
  },
  assignmentStatusText: {
    fontWeight: "bold"
  },
  settingsMenu: {
    borderRadius: "20px",
    width: "300px",
    backgroundColor: "pink"
  },
  settingsMenuItem: {
    borderBottom: "1px solid",
    width: "200px",
    justifyContent: "center"
  }
}));

export default function AssignmentStatus(props) {
  const [state, setState] = useContext(TaskCardContext);
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }
  function handleMenuClose() {
    setAnchorEl(null);
  }

  return (
    <div>
      <div className={classes.assignmentStatusRoot}>
        <IconButton
          data-testid="assignmentStatusButton"
          className={classes.assignmentStatusButton}
          aria-label="assignment settings"
          onClick={handleClick}
        >
          <SettingsIcon />
        </IconButton>
        <Typography className={classes.assignmentStatusText}>
          {state.assigned > 0 ? "Running" : "Unassigned"}
        </Typography>
      </div>
      <StatusMenu anchorEl={anchorEl} handleMenuClose={handleMenuClose} />
    </div>
  );
}
