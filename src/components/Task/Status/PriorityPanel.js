import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { IconButton } from "@material-ui/core";
import StarTwoToneIcon from "@material-ui/icons/StarBorderTwoTone";
import StarIcon from "@material-ui/icons/Star";
import { TaskCardContext } from "../TaskCard";

export default function PriorityPanel(props) {
  const { onPrioritySelected } = props;
  const [state, setState] = useContext(TaskCardContext);
  const handleClick = value => {
    setState({ ...state, priorityValue: value });
    setTimeout(() => {
      setState(state => {
        setState({ ...state, assigned: 6 });
      });
    }, 2000);
    onPrioritySelected(value);
  };
  const assignWorkers = theState => {};
  const useStyles = makeStyles(theme => ({
    priorityRoot: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center"
    },
    priorityButtons: {
      padding: "0px",
      pointerEvents: props.disablePointerEvents ? "none" : "all"
    },
    priorityStarTwoTone: {
      color: "grey",
      fontSize: "36px"
    },
    priorityStarFill: {
      color: "#FFCE2B",
      fontSize: "36px"
    }
  }));

  const PriorityStar = props => {
    return (
      <div className={classes.priorityRoot}>
        <IconButton
          // inputProps={{ "data-testid": "priority-one" }}
          data-testid={"priority-" + props.position}
          className={classes.priorityButtons}
          aria-label="set priority "
          onClick={evt => handleClick(props.position)}
        >
          {props.position <= props.priorityValue ? (
            <StarIcon className={classes.priorityStarFill} />
          ) : (
            <StarTwoToneIcon className={classes.priorityStarTwoTone} />
          )}
        </IconButton>
      </div>
    );
  };

  const classes = useStyles();
  return (
    <div className={classes.priorityRoot}>
      <PriorityStar position={1} priorityValue={state.priorityValue} />
      <PriorityStar
        data-testid="priority-two"
        position={2}
        priorityValue={state.priorityValue}
      />
      <PriorityStar
        data-testid="priority-three"
        position={3}
        priorityValue={state.priorityValue}
      />
    </div>
  );
}
