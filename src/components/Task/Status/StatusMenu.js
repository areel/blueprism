import React, { useState, useContext } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import PriorityPanel from "./PriorityPanel";
import { TaskCardContext } from "../TaskCard";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  priorityMenu: {
    textAlign: "center",
    fontSize: "20px"
  }
}));

export default function StatusMenu(props) {
  const classes = useStyles();
  const { anchorEl, handleMenuClose } = props;
  const [state, setState] = useContext(TaskCardContext);
  const [showPriorityMenu, setShowPriorityMenu] = useState(state.assigned);

  const StyledMenu = withStyles({
    paper: {
      borderRadius: "10px",
      border: "1px solid #d3d4d5",
      display: "flex",
      justifyContent: "center",
      color: "#0C75B7"
    }
  })(props => (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center"
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "center"
      }}
      {...props}
    />
  ));

  const StyledMenuItem = withStyles(theme => ({
    root: {
      width: "200px",
      justifyContent: "center",
      "&:hover": {
        backgroundColor: "#0C75B7",
        color: "white"
      }
    }
  }))(MenuItem);

  const StyledDivider = withStyles(theme => ({
    root: {
      backgroundColor: "#0C75B7"
    }
  }))(Divider);

  function handleAutoAssign() {
    setShowPriorityMenu(true);
  }

  function handleStatusMenuClose(_, priorityValue) {
    const payload = isNaN(priorityValue) ? null : { priorityValue };
    if (payload == null) {
      setState({ ...state, assigned: 0, priorityValue: 0 });
      setShowPriorityMenu(false);
    }
    handleMenuClose();
  }

  function handlePrioritySelected(priorityValue) {
    handleStatusMenuClose(null, priorityValue);
  }
  const AssignmentMenu = () => {
    return (
      <div>
        <StyledMenuItem
          data-testid="auto-assign-menu-item"
          onClick={handleAutoAssign}
        >
          Auto-assign
        </StyledMenuItem>
        <StyledDivider />
        <StyledMenuItem onClick={handleStatusMenuClose}>
          Select workers
        </StyledMenuItem>
        <StyledDivider />
        <StyledMenuItem onClick={handleStatusMenuClose}>
          Schedule
        </StyledMenuItem>
        <StyledDivider />
        <StyledMenuItem onClick={handleStatusMenuClose}>Archive</StyledMenuItem>
      </div>
    );
  };

  const PriorityMenu = () => {
    return (
      <div>
        <Typography className={classes.priorityMenu}>Set priority</Typography>
        <PriorityPanel onPrioritySelected={handlePrioritySelected} />
      </div>
    );
  };
  return (
    <StyledMenu
      id="simple-menu"
      classes={{}}
      anchorEl={anchorEl}
      keepMounted
      open={Boolean(anchorEl)}
      onClose={handleStatusMenuClose}
    >
      {showPriorityMenu ? PriorityMenu() : AssignmentMenu()}
    </StyledMenu>
  );
}
