import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  progressTextRoot: {
    minWidth: "300px",
    textAlign: "center"
  }
}));

export default function ProgressText(props) {
  const classes = useStyles();
  const { className, ...other } = props;
  return (
    <div className={`${className} ${classes.progressTextRoot}`}>
      <div style={{ lineHeight: "10px", display: "grid" }}>
        <Typography
          style={{
            fontSize: "10px",
            gridRow: 1,
            gridColumn: 1
          }}
          variant="caption"
        >
          Tasks in queue: 6
        </Typography>

        <Typography
          style={{ fontSize: "10px", gridRow: 1, gridColumn: 2 }}
          variant="caption"
        >
          Average task time: 12s
        </Typography>
      </div>
      <div style={{ lineHeight: "10px", display: "grid" }}>
        <Typography
          style={{ fontSize: "10px", gridRow: 1, gridColumn: 1 }}
          variant="caption"
        >
          Tasks completed: 18
        </Typography>

        <Typography
          style={{ fontSize: "10px", gridRow: 1, gridColumn: 2 }}
          variant="caption"
        >
          Time remaining: 01:12
        </Typography>
      </div>
    </div>
  );
}
