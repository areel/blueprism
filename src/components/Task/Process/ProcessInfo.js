import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
const useStyles = makeStyles(theme => ({
  processInfoRoot: {
    display: "flex",
    flexDirection: "column"
  },
  processInfoNameLabel: {
    color: "grey",
    fontSize: "15px",
    marginLeft: "10px",
    paddingBottom: 0
  },
  processName: {
    fontSize: "20px",
    marginTop: "-10px",
    marginLeft: "10px",
    marginRight: "30px",
    borderBottom: "1px solid"
  },
  processDescription: {
    fontSize: "16px",
    marginLeft: "10px"
  }
}));

export default function ProcessInfo() {
  const classes = useStyles();
  return (
    <div className={classes.processInfoRoot}>
      <Typography className={classes.processInfoNameLabel}>Name</Typography>
      <Typography className={classes.processName}>
        Order coffee beans
      </Typography>
      <Typography className={classes.processDescription}>
        Process description
      </Typography>
    </div>
  );
}
