import React, { useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { IconButton } from "@material-ui/core";
import PauseIcon from "@material-ui/icons/PauseCircleOutline";
import PlayIcon from "@material-ui/icons/PlayCircleOutline";
import { TaskCardContext } from "../TaskCard";

const useStyles = makeStyles(theme => ({
  root: {
    display: "grid",
    gridTemplateColumns: "auto",
    gridTemplateRows: "auto auto auto"
  },
  topVerticalDivider: {
    gridColumnStart: "1",
    gridRowStart: "1",
    borderLeft: "1px solid #38546d",
    width: "1px",
    height: "45px",
    marginLeft: "14px",
    zIndex: "0"
  },
  playButton: {
    fontSize: "30px",
    color: "black"
  },
  iconButton: {
    gridColumnStart: "1",
    gridRowStart: "2",
    padding: 0
  },
  bottomVerticalDivider: {
    gridColumnStart: "1",
    gridRowStart: "3",
    borderLeft: "1px solid #38546d",
    width: "1px",
    height: "45px",
    marginLeft: "14px",
    zIndex: "0"
  }
}));

export default function ProcessPlayPause(props) {
  const [state, setState] = useContext(TaskCardContext);
  const classes = useStyles();
  const { className, ...other } = props;

  const executeTask = () => {
    if (state.priorityValue) {
      setState({ ...state, assigned: 7 });
    }
  };

  const pauseTask = () => {
    setState({ ...state, assigned: 0 });
  };

  return (
    <div className={`${className} ${classes.root}`}>
      <div className={classes.topVerticalDivider}> </div>
      {state.priorityValue > 0 && state.assigned > 0 ? (
        <IconButton
          data-testid="pause-button"
          className={classes.iconButton}
          onClick={pauseTask}
          aria-label="Pause Task"
        >
          <PauseIcon className={classes.playButton} />
        </IconButton>
      ) : (
        <IconButton
          data-testid="play-button"
          className={classes.iconButton}
          onClick={executeTask}
          aria-label="Execute Task"
        >
          <PlayIcon className={classes.playButton} />
        </IconButton>
      )}
      <div className={classes.bottomVerticalDivider}> </div>
    </div>
  );
}
