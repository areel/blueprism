import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ProcessInfo from "./ProcessInfo";
import ProcessProgress from "./ProcessProgress";

const useStyles = makeStyles(theme => ({
  processCardRoot: {
    width: "80%",
    minWidth: "400px"
  }
}));

export default function ProcessCard(props) {
  const classes = useStyles();
  const { className, ...other } = props;
  return (
    <div className={`${className} ${classes.processCardRoot}`}>
      <ProcessInfo />
      <ProcessProgress />
    </div>
  );
}
