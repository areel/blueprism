import React, { useContext } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { IconButton } from "@material-ui/core";
import ToggleIcon from "@material-ui/icons/RemoveRedEye";
import LinearProgress from "@material-ui/core/LinearProgress";
import ProgressText from "./ProgressText";
import { TaskCardContext } from "../TaskCard";

const useStyles = makeStyles(theme => ({
  processProgressRoot: {
    display: "flex",
    flexDirection: "row",
    marginTop: "10px"
  },
  toggleIcon: {
    color: "#005285"
  },
  margin: {
    margin: theme.spacing(1)
  },
  container_row: {
    flexGrow: 1,
    display: "grid",
    marginRight: "30px"
  },
  layer1: {
    zIndex: 1,
    alignSelf: "center",
    gridColumn: 1,
    gridRow: 1
  },
  layer2: {
    zIndex: 2,
    alignSelf: "center",
    gridColumn: 1,
    gridRow: 1
  }
}));

const BorderLinearProgress = withStyles({
  root: {
    height: 30,
    backgroundColor: "#D0EDFF",
    borderRadius: 10
  },
  bar: {
    borderRadius: 10,
    backgroundColor: "#0C75B7"
  }
})(LinearProgress);

export default function ProcessProgress(props) {
  const [state, setState] = useContext(TaskCardContext);
  const { className, ...other } = props;
  const classes = useStyles();
  return state.priorityValue ? (
    <div className={`${className} ${classes.processProgressRoot}`}>
      <IconButton className={classes.toggleIcon} aria-label="Toggle Metrics">
        <ToggleIcon />
      </IconButton>
      <div className={classes.container_row}>
        <BorderLinearProgress
          className={classes.layer1}
          variant="determinate"
          value={state.assigned ? 25 : 0}
        />
        {state.assigned ? <ProgressText className={classes.layer2} /> : null}
      </div>
    </div>
  ) : null;
}
