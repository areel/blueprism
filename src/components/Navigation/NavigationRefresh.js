import React from "react";
import { IconButton } from "@material-ui/core";
import RefreshIcon from "@material-ui/icons/Autorenew";

const NavigationRefresh = function(props) {
  return (
    <IconButton color="inherit" aria-label="Open Drawer">
      <RefreshIcon />
    </IconButton>
  );
};
export default NavigationRefresh;
