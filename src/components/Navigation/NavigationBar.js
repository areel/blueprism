import React from "react";
import { createStyles, withStyles, WithStyles } from "@material-ui/core/styles";
import NavigationMenu from "../Navigation/NavigationMenu";
import NavigationRefresh from "../Navigation/NavigationRefresh";
import TaskSearch from "../Search/TaskSearch";
import { Toolbar } from "@material-ui/core";

const styles = theme =>
  createStyles({
    toolBar: {
      borderBottom: "1px solid",
      marginLeft: "5%",
      paddingLeft: 0,
      marginRight: "5%",
      paddingRight: 0,
      color: "#005285"
    },
    centerSearch: {
      margin: "auto",
      width: "50%"
    }
  });

const NavigationBar = function(props) {
  return (
    <div>
      <Toolbar className={props.classes.toolBar}>
        <NavigationMenu />
        <TaskSearch />
        <NavigationRefresh />
      </Toolbar>
    </div>
  );
};

export default withStyles(styles)(NavigationBar);
