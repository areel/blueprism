import React from "react";
import { IconButton } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";

const NavigationMenu = function(props) {
  return (
    <IconButton color="inherit" aria-label="Open Drawer">
      <MenuIcon />
    </IconButton>
  );
};
export default NavigationMenu;
