import React from "react";
import { createStyles, withStyles } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";

const styles = theme =>
  createStyles({
    inputRoot: {
      color: "#D4D4D4",
      width: "100%"
    },
    inputInput: {
      paddingTop: theme.spacing(1),
      paddingRight: theme.spacing(0),
      paddingBottom: theme.spacing(1),
      paddingLeft: theme.spacing(4)
    },
    search: {
      position: "relative",
      borderBottom: "1px solid #D4D4D4",
      margin: "auto",
      width: "45%"
    },
    searchIcon: {
      width: theme.spacing(9),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center"
    }
  });

const TaskSearch = props => {
  return (
    <div data-testid="task-search" className={props.classes.search}>
      <div className={props.classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Search processes, workers, tags"
        classes={{
          root: props.classes.inputRoot,
          input: props.classes.inputInput
        }}
      />
    </div>
  );
};

export default withStyles(styles)(TaskSearch);
