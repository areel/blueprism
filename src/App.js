import React from "react";
import NavigationBar from "./components/Navigation/NavigationBar";
import { TaskCard } from "./components/Task/TaskCard";

function App() {
  return (
    <div>
      <NavigationBar />
      <TaskCard card={{ priorityValue: 1, assigned: 13 }} />
      <TaskCard card={{ priorityValue: 0, assigned: 0 }} />
    </div>
  );
}

export default App;
