const path = require("path");
const include = path.join(__dirname, "src");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  output: {
    filename: "[name].[chunkhash].js",
    path: path.resolve(__dirname, "dist"),
    publicPath: "/"
  },
  devtool: "source-map",
  devServer: {
    port: 8081
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: [
          {
            loader: "babel-loader",
            options: {
              babelrc: false,
              presets: ["@babel/preset-react", "@babel/preset-env"],
              plugins: ["istanbul"]
            }
          }
        ],
        include
      },
      {
        test: /\.module.css$/,
        loaders: [
          "style-loader",
          {
            loader: "css-loader"
          },
          "postcss-loader"
        ],
        include
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: {
          loader: "file-loader?name=img/[name].[ext]"
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf)$/,
        loader: "file-loader",
        options: {
          name: "../assets/fonts/[name].[ext]"
        },
        include
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      favicon: "./src/assets/img/favicon.ico",
      template: "./src/index.html"
    }),
    new CleanWebpackPlugin()
  ]
};
